const express = require("express");
const Vigenere = require("caesar-salad").Vigenere;
const password = "LittleKing";

const app = express();

const port = 8000;

app.get("/:text", (req, res) => {
  res.send(req.params.text);
});

app.get("/encode/:text", (req, res) => {
  res.send(Vigenere.Cipher(password).crypt(req.params.text));
});
app.get("/decode/:text", (req, res) => {
  res.send(Vigenere.Decipher(password).crypt(req.params.text));
});

app.listen(port, () => {
  console.log("We are live a port " + port);
});
